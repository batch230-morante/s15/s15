console.log("Hello World!");

let userDetails = {
    firstName: 'John',
    lastName: 'Morante',
    age: 25,
    hobbies: ['watching anime', 'playing video games', 'watching content online'],
    workAddress: {
        houseNumber: '777',
        street: '123 street',
        city: 'Manila',
        state: 'Metro Manila'
    }
}

let firstName = 'John';
console.log("First Name: " + firstName);

let lastName = 'Smith';
console.log("Last Name: " + lastName);

let age = 30;
console.log("Age: " + age);

const mattHobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
console.log('Hobbies:');
console.log(mattHobbies);

workAddress = {
    houseNumber: '32',
    street: 'Washington Street',
    city: 'Lincoln',
    state: 'Nebraska'
}
console.log('Work Address:');
console.log(workAddress);


console.log(">> Debugging Practice");

let characterName = "Steve Rogers";
	console.log("My full name is " + characterName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);